MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_DBNAME = 'apitest'
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']
#RESOURCE_METHODS = ['GET', 'POST', 'DELETE']

users = {
  "cache_expires": 10,
  "resource_methods": [
    "GET",
    "POST",
    "DELETE"
  ],
  "item_title": "users",
  "additional_lookup": {
    "url": "regex(\"[\\w]+\")",
    "field": "username"
  },
  "cache_control": "max-age=10,must-revalidate",
  "schema": {
    "username": {
      "type": "string",
      "required": True,
      "unique": True,
      "minlength": 1,
      "maxlength": 35
    },
    "firstname": {
      "minlength": 1,
      "type": "string",
      "required": True,
      "maxlength": 35
    },
    "lastname": {
      "minlength": 1,
      "type": "string",
      "required": True,
      "maxlength": 35
    },
    "role": {
      "type": "list",
      "allowed": [
        "administrator",
        "contributor",
        "viewer"
      ]
    },
    "location": {
      "type": "dict",
      "schema": {
        "city": {
          "type": "string"
        },
        "country": {
          "type": "string"
        },
        "address": {
          "type": "string"
        }
      }
    },
    "born": {
      "type": "datetime"
    }
  }
}

DOMAIN = {
  "users": users,
  "tasks": {
    "allow_unknown": True,
    "schema": {
      "owner": {
        "type": "objectid",
        "data_relation": {
          "embeddable": True,
          "resource": "users"
        },
        "required": True
      },
      "description": {
        "type": "string"
      },
      "title": {
        "type": "string",
        "required": True
      }
    }
  }
}


