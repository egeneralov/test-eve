Sample eve api
==============

### Errors

- any answer returns `_status` field
-- OK


### Create user:

Payload:

- username (required, uniq)
- firstname (required)
- lastname (required)
- role "administrator","contributor","viewer"
- location
  - address
  - city
  - country

#### Request

```bash
curl -s http://127.0.0.1:5000/users \
-H 'Content-Type: application/json' \
-d '{
  "username": "egeneralov",
  "firstname": "Eduard",
  "lastname": "Generalov",
  "role": ["viewer"],
  "location": {
    "address": "Red Square",
    "city": "Moscow",
    "country": "Russian Federation"
  }
}' | jq
```

#### Answer:

- _id - permanent id for record
- _created - creation date
- _updated - last record update
- _etag - id for current record state

```json
  {
    "_etag": "ccd2d171d7fde93a938806a63692add50a565fa2",
    "_status": "OK",
    "_links": {
      "self": {
        "href": "users/5b7c8a8c5f56b2463d098024",
        "title": "users"
      }
    },
    "_created": "Tue, 21 Aug 2018 21:56:28 GMT",
    "_updated": "Tue, 21 Aug 2018 21:56:28 GMT",
    "_id": "5b7c8a8c5f56b2463d098024"
  }
```

## Get user info

All fields from creation

#### Request

```bash
# username
curl -s http://127.0.0.1:5000/users/egeneralov | jq
# _id
curl -s http://127.0.0.1:5000/users/5b7c8a8c5f56b2463d098024 | jq
```

#### Answer

```json
{
  "lastname": "Generalov",
  "role": [
    "viewer"
  ],
  "_links": {
    "self": {
      "href": "users/5b7c8a8c5f56b2463d098024",
      "title": "users"
    },
    "collection": {
      "href": "users",
      "title": "users"
    },
    "parent": {
      "href": "/",
      "title": "home"
    }
  },
  "location": {
    "address": "Red Square",
    "country": "Russian Federation",
    "city": "Moscow"
  },
  "_updated": "Tue, 21 Aug 2018 21:56:28 GMT",
  "firstname": "Eduard",
  "_created": "Tue, 21 Aug 2018 21:56:28 GMT",
  "_etag": "ccd2d171d7fde93a938806a63692add50a565fa2",
  "username": "egeneralov",
  "_id": "5b7c8a8c5f56b2463d098024"
}
```


## Tasks

#### Request

```bash
curl -s http://127.0.0.1:5000/tasks -H 'Content-Type: application/json' -d '{
  "owner": "5b7c8a8c5f56b2463d098024",
  "title": "My first task",
  "description": "My cool description",
  "images": [
    "http://google.com/logo.png",
    "http://ya.ru/logo.png"
  ]
}' | jq
```

#### Answer

```json
{
  "_links": {
    "self": {
      "href": "tasks/5b7c94eb5f56b2476a0ac86c",
      "title": "Task"
    }
  },
  "_updated": "Tue, 21 Aug 2018 22:40:43 GMT",
  "_id": "5b7c94eb5f56b2476a0ac86c",
  "_created": "Tue, 21 Aug 2018 22:40:43 GMT",
  "_etag": "2484863887b718915c4e2f3e81c053cb23a9d659",
  "_status": "OK"
}
```

#### Request

- url must be endpoint (http://127.0.0.1:5000) + domain (users) + record _id (5b7c8a8c5f56b2463d098024)
- Must be present header `If-Match` with current record `_etag`

```bash
curl -s -X PATCH http://127.0.0.1:5000/users/5b7c8a8c5f56b2463d098024
-H "Content-Type: application/json"
-H 'If-Match: ccd2d171d7fde93a938806a63692add50a565fa2'
-d '{"firstname": "ronald"}' | jq
```

#### Answer

```json
{
  "_links": {
    "self": {
      "title": "users",
      "href": "users/5b7c8a8c5f56b2463d098024"
    }
  },
  "_created": "Tue, 21 Aug 2018 21:56:28 GMT",
  "_etag": "c977b8de24c8b54af1bac4a6cb39a61098556044",
  "_status": "OK",
  "_id": "5b7c8a8c5f56b2463d098024",
  "_updated": "Tue, 21 Aug 2018 22:53:18 GMT"
}
```

#### Errors

- Empty answer with code 422: not current record `_etag`
- answer code 428: To edit a document its etag must be provided using the If-Match header














